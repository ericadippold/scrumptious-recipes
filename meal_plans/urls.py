from django.urls import path

from meal_plans.views import (
    Meal_PlansCreateView,
    Meal_PlansDetailView,
    Meal_PlansListView,
    Meal_PlansUpdateView,
    Meal_PlansDeleteView
)


urlpatterns = [
    path("", Meal_PlansListView.as_view(), name="meal_plans_list"),
    path("create/", Meal_PlansCreateView.as_view(), name="meal_plans_new"),
    path("<int:pk>/", Meal_PlansDetailView.as_view(), name="meal_plans_detail"),
    path("<int:pk>/edit/", Meal_PlansUpdateView.as_view(), name="meal_plans_edit"),
    path("<int:pk>/delete/", Meal_PlansDeleteView.as_view(), name="meal_plans_delete"),
]
