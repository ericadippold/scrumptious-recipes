from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


from meal_plans.models import Meal_Plan

# Create your views here.

class Meal_PlansCreateView(LoginRequiredMixin, CreateView):
    model = Meal_Plan
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

class Meal_PlansUpdateView(UpdateView):
    model = Meal_Plan
    context_object_name = "meal_plans"
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)


class Meal_PlansListView(ListView):
    paginate_by = 4
    model = Meal_Plan
    context_object_name = "meal_plans"
    template_name = "meal_plans/list.html"

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)



class Meal_PlansDetailView(DetailView):
    model = Meal_Plan
    context_object_name = "meal_plans"
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)


class Meal_PlansDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal_Plan
    context_object_name = "meal_plans"
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)
