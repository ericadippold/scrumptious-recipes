# Generated by Django 4.0.3 on 2022-04-29 23:32

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0016_recipe_servings_alter_shoppingitem_food_item_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='servings',
            field=models.PositiveSmallIntegerField(null=True, validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]
