from pprint import pprint
from django import template

register = template.Library()

def resize_to(ingredient, target):
    #get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings properties
    servings = ingredient.recipe.servings
    amount = ingredient.amount
    # if the servings from the recipe is not None
        # and the value of the target is not None
    if servings is not None and target is not None:
        try:
            ratio = int(target) / int(servings)
            servings = ratio * amount
            return servings
        except:
            pass
    return amount
            #try
                #calculate the ratio of target over servings
                #return the ratio multiplied by the ingredients amount
            # catch a possible error
                # pass
    # return the original ingredient's amount since nothing else worked

    #print("value:", value)
    #print("arg:", arg)
    #return "resize done"

register.filter(resize_to)


#writing
