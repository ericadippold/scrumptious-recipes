from pprint import pprint
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.decorators.http import require_http_methods
from django.contrib.auth.models import User


from recipes.forms import RatingForm


from recipes.models import USER_MODEL, Ingredient, Recipe, ShoppingItem




class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

class RecipeListView(ListView):
    paginate_by = 4
    model = Recipe
    template_name = "recipes/list.html"

class RecipeAuthorListView(ListView):
    paginate_by = 4
    model = User
    template_name = "recipes/users.html"
    context_object_name = "users"


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        #Create a new empty list and assign it to a variable
        foods = []
        # For each item in the user's shopping items, which we
        # can access through the code.
        # self.request.user.shopping_items.all()
        for item in self.request.user.shopping_items.all():
        #test1 = self.request.user.shopping_items.all()
        #test2 = self.request.shopping_item.user
            #Add the shopping item's food to the list
            foods.append(item.food_item)
        # the self.request.GET property is a dictionary
        #get the value out of there associated w/ the key "servings"
        # store in the context dictionary w/ the key servings
        context["servings"] = self.request.GET.get("servings")

        # Put that list into the context
        context["food_in_shopping_list"] = foods

        return context


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")

def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)

#view 1 -- Create
    # function form
@require_http_methods(["POST"])
def create_shopping_item(request):
    #get the value for the "ingredient_id" from the
    # request.POST dictionary using the "get" method
    ingredient_id = request.POST.get("ingredient_id")
    #get the specific ingredient from the Ingredient model
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # get the current user
    user = request.user
    try:
        #Create the new shopping item in the database
        ShoppingItem.objects.create(food_item=ingredient.food,user=user)
    #catch the error if its already in there
    except IntegrityError:
        #Don't do anything w/ the error
        pass
    #Go back to the recipe page
    return redirect("recipe_detail", pk=ingredient.recipe.id)

#view 2 -- List
    # class form
    #wlil show a list of shopping items that was created by the user. Not all shopping items
class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)

#view 3 -- Delete
    # function form

def delete_all_shopping_items(request):
    #Delete all of the shopping items for the user
    #using code like
    #ShoppingItem.objects.filter(user=the current user).delete()

    #GO back to the shopping item list with a redirect
    #to the name of the registered shopping item list
    # path with code like this
    # return redirect(
    # name of the reqistered shopping item list path
    # )

    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_item_list")
